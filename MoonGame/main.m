//
//  main.m
//  MoonGame
//
//  Created by Dahiri Farid on 12/24/14.
//  Copyright (c) 2014 Dahiri Farid. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
