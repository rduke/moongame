//
//  ViewController.h
//  MoonGame
//
//  Created by Dahiri Farid on 12/24/14.
//  Copyright (c) 2014 Dahiri Farid. All rights reserved.
//

#import <UIKit/UIKit.h>

@class OgreView;

@interface ViewController : UIViewController

- (void)startWithWindow:(UIWindow*) window;
- (void)update:(id)sender;
- (void)stop;
- (IBAction)pinch:(UIPinchGestureRecognizer *)sender;
- (IBAction)rotate:(UIRotationGestureRecognizer *)sender;
- (IBAction)resetCameraButton:(UIButton *)sender;


@end

