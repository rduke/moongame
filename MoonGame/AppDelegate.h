//
//  AppDelegate.h
//  MoonGame
//
//  Created by Dahiri Farid on 12/24/14.
//  Copyright (c) 2014 Dahiri Farid. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow* mWindow;
@property (strong, nonatomic) ViewController* mViewController;


@end

