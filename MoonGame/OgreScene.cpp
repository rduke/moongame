//
//  OgreScene.cpp
//  MoonGame
//
//  Created by Dahiri Farid on 3/14/15.
//  Copyright (c) 2015 Dahiri Farid. All rights reserved.
//

#include "OgreScene.h"
#include <OgreSceneNode.h>
#include <OgreMaterialManager.h>
#include <OgreRenderWindow.h>
#include <OgreEntity.h>

void getTerrainImage(bool flipX, bool flipY, Ogre::Image& img);

void getTerrainImage(bool flipX, bool flipY, Ogre::Image& img)
{
    img.load("terrain.png", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
    if (flipX)
        img.flipAroundY();
    if (flipY)
        img.flipAroundX();
    
}

OgreScene::OgreScene(Ogre::Root* aRoot,
                     Ogre::SceneManager* aSceneMgr)
:mRoot(aRoot),
 mSceneManager(aSceneMgr),
 mCamera(nullptr),
 mViewport(nullptr)
{
    
}

void OgreScene::start()
{
    mSceneManager->setDisplaySceneNodes(true);
    Ogre::MaterialManager::getSingleton().setDefaultTextureFiltering(Ogre::TFO_ANISOTROPIC);
    Ogre::MaterialManager::getSingleton().setDefaultAnisotropy(7);
    
    Ogre::Vector3 lightdir(0.55, -0.3, 0.75);
    lightdir.normalise();
    
    Ogre::Light* light = mSceneManager->createLight("tstLight");
    light->setType(Ogre::Light::LT_DIRECTIONAL);
    light->setDirection(lightdir);
    light->setDiffuseColour(Ogre::ColourValue::White);
    light->setSpecularColour(Ogre::ColourValue(0.4, 0.4, 0.4));
    
    mSceneManager->setAmbientLight(Ogre::ColourValue(0.2, 0.2, 0.2));
    
    mTerrainGlobals = OGRE_NEW Ogre::TerrainGlobalOptions();
    
    mTerrainGroup = OGRE_NEW Ogre::TerrainGroup(mSceneManager, Ogre::Terrain::ALIGN_X_Z, 513, 12000.0f);
    mTerrainGroup->setFilenameConvention(Ogre::String("BasicTutorial3Terrain"), Ogre::String("dat"));
    mTerrainGroup->setOrigin(Ogre::Vector3::ZERO);
    
    configureTerrainGlobals(light);
    
    for (long x = 0; x <= 0; ++x)
        for (long y = 0; y <= 0; ++y)
            defineTerrain(x, y);
    
    // sync load since we want everything in place when we start
    mTerrainGroup->loadAllTerrains(true);
    
    if (mTerrainsImported)
    {
        Ogre::TerrainGroup::TerrainIterator ti = mTerrainGroup->getTerrainIterator();
        while(ti.hasMoreElements())
        {
            Ogre::Terrain* t = ti.getNext()->instance;
            initBlendMaps(t);
        }
    }
    
    mTerrainGroup->freeTemporaryResources();
 
    createLander();
 
    createCamera();
}

void OgreScene::updateCamera(Ogre::Radian aRoll, Ogre::Radian aYaw)
{
    mCameraPitchSceneNode->roll(aRoll, Ogre::Node::TS_LOCAL);
    mCameraYawSceneNode->yaw(aYaw);
}

void OgreScene::createLander()
{
    mLander = mSceneManager->createEntity("Lander", "ogrehead.mesh");
    mLanderSceneNode = mSceneManager->getRootSceneNode()->createChildSceneNode("LanderNode");
    mLanderSceneNode->attachObject(mLander);
    
    mLanderSceneNode->setPosition(0.0f, 500.0f, 0.0f);
}

void OgreScene::createCamera()
{
    mCamera = mSceneManager->createCamera("LanderCamera");
    
    Ogre::RenderWindow* renderWindow = (Ogre::RenderWindow*)mRoot->getRenderTarget("RenderWindow");

    mViewport = renderWindow->addViewport(mCamera);
    mViewport->setBackgroundColour(Ogre::ColourValue(0.8f, 0.7f, 0.6f, 1.0f));
    mViewport->setCamera(mCamera);

    mCamera->setPosition(Ogre::Vector3::ZERO);
    mCamera->setNearClipDistance(1.0);
    mCamera->setFarClipDistance(50000);
    mCamera->setAspectRatio(Ogre::Real(mViewport->getActualWidth()) /
                            Ogre::Real(mViewport->getActualHeight()));
    
    if (mRoot->getRenderSystem()->getCapabilities()->hasCapability(Ogre::RSC_INFINITE_FAR_PLANE))
    {
        mCamera->setFarClipDistance(0);   // enable infinite far clip distance if we can
    }
    
    mCameraContainerSceneNode=mSceneManager->createSceneNode("CameraContainerSceneNode");
    mCameraPitchSceneNode   = mSceneManager->createSceneNode("CameraPitchSceneNode");
    mCameraYawSceneNode     = mSceneManager->createSceneNode("CameraYawSceneNode");
    mCameraSceneNode        = mSceneManager->createSceneNode("CameraSceneNode");
    
    mLanderSceneNode->addChild(mCameraContainerSceneNode);
    
    mCameraContainerSceneNode->addChild(mCameraYawSceneNode);
    
    mCameraYawSceneNode->addChild(mCameraPitchSceneNode);
    
    mCameraPitchSceneNode->addChild(mCameraSceneNode);
    
    mCameraSceneNode->attachObject(mCamera);
    
    this->resetCamera();
}

void OgreScene::resetCamera(void)
{
    mCameraYawSceneNode->resetToInitialState();
    mCameraPitchSceneNode->resetToInitialState();
    const Ogre::AxisAlignedBox& box = mLander->getBoundingBox();
    Ogre::Vector3 size = box.getSize();
    
    mCameraPitchSceneNode->setPosition(455.0, 0.0, 0.0);
    
    mCamera->setPosition(Ogre::Vector3::ZERO);
    mCamera->lookAt(mLanderSceneNode->_getDerivedPosition().x - size.x / 2.0f,
                    mLanderSceneNode->_getDerivedPosition().y + size.y / 2.0f,
                    mLanderSceneNode->_getDerivedPosition().z - size.z / 2.0f);
}

void OgreScene::configureTerrainGlobals(Ogre::Light* aLight)
{
    // Configure global
    mTerrainGlobals->setMaxPixelError(8);
    // testing composite map
    mTerrainGlobals->setCompositeMapDistance(3000);
    
    // Important to set these so that the terrain knows what to use for derived (non-realtime) data
    mTerrainGlobals->setLightMapDirection(aLight->getDerivedDirection());
    mTerrainGlobals->setCompositeMapAmbient(mSceneManager->getAmbientLight());
    mTerrainGlobals->setCompositeMapDiffuse(aLight->getDiffuseColour());
    
    // Configure default import settings for if we use imported image
    Ogre::Terrain::ImportData& defaultimp = mTerrainGroup->getDefaultImportSettings();
    defaultimp.terrainSize = 513;
    defaultimp.worldSize = 12000.0f;
    defaultimp.inputScale = 600;
    defaultimp.minBatchSize = 33;
    defaultimp.maxBatchSize = 65;
    // textures
    defaultimp.layerList.resize(3);
    defaultimp.layerList[0].worldSize = 100;
    defaultimp.layerList[0].textureNames.push_back("dirt_grayrocky_diffusespecular.dds");
    defaultimp.layerList[0].textureNames.push_back("dirt_grayrocky_normalheight.dds");
    defaultimp.layerList[1].worldSize = 30;
    defaultimp.layerList[1].textureNames.push_back("grass_green-01_diffusespecular.dds");
    defaultimp.layerList[1].textureNames.push_back("grass_green-01_normalheight.dds");
    defaultimp.layerList[2].worldSize = 200;
    defaultimp.layerList[2].textureNames.push_back("growth_weirdfungus-03_diffusespecular.dds");
    defaultimp.layerList[2].textureNames.push_back("growth_weirdfungus-03_normalheight.dds");
}

void OgreScene::defineTerrain(long aX, long aY)
{
    Ogre::String filename = mTerrainGroup->generateFilename(aX, aY);
    if (Ogre::ResourceGroupManager::getSingleton().resourceExists(mTerrainGroup->getResourceGroup(), filename))
    {
        mTerrainGroup->defineTerrain(aX, aY);
    }
    else
    {
        Ogre::Image img;
        getTerrainImage(aX % 2 != 0, aY % 2 != 0, img);
        mTerrainGroup->defineTerrain(aX, aY, &img);
        mTerrainsImported = true;
    }
}

void OgreScene::initBlendMaps(Ogre::Terrain* terrain)
{
    Ogre::TerrainLayerBlendMap* blendMap0 = terrain->getLayerBlendMap(1);
    Ogre::TerrainLayerBlendMap* blendMap1 = terrain->getLayerBlendMap(2);
    Ogre::Real minHeight0 = 70;
    Ogre::Real fadeDist0 = 40;
    Ogre::Real minHeight1 = 70;
    Ogre::Real fadeDist1 = 15;
    float* pBlend0 = blendMap0->getBlendPointer();
    float* pBlend1 = blendMap1->getBlendPointer();
    for (Ogre::uint16 y = 0; y < terrain->getLayerBlendMapSize(); ++y)
    {
        for (Ogre::uint16 x = 0; x < terrain->getLayerBlendMapSize(); ++x)
        {
            Ogre::Real tx, ty;
            
            blendMap0->convertImageToTerrainSpace(x, y, &tx, &ty);
            Ogre::Real height = terrain->getHeightAtTerrainPosition(tx, ty);
            Ogre::Real val = (height - minHeight0) / fadeDist0;
            val = Ogre::Math::Clamp(val, (Ogre::Real)0, (Ogre::Real)1);
            *pBlend0++ = val;
            
            val = (height - minHeight1) / fadeDist1;
            val = Ogre::Math::Clamp(val, (Ogre::Real)0, (Ogre::Real)1);
            *pBlend1++ = val;
        }
    }
    blendMap0->dirty();
    blendMap1->dirty();
    blendMap0->update();
    blendMap1->update();
}

void OgreScene::update(double aTimeSinceLastFrame)
{
    const Ogre::AxisAlignedBox& box = mLander->getBoundingBox();
    Ogre::Vector3 size = box.getSize();
    
    getCameraSceneNode()->setPosition(aTimeSinceLastFrame * getZoomSpeed(),
                                      size.y / 2.0f,
                                      size.z / 2.0f);
}
