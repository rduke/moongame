//
//         DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
//                     Version 2, December 2004
//
//  Copyright (C) 2013 Clodéric Mars <cloderic.mars@gmail.com>
//
//  Everyone is permitted to copy and distribute verbatim or modified
//  copies of this license document, and changing it is allowed as long
//  as the name is changed.
//
//             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
//    TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
//
//   0. You just DO WHAT THE FUCK YOU WANT TO.

#ifndef OGRE_DEMO_H
#define OGRE_DEMO_H

#include <OgreCamera.h>
#include <OgreEntity.h>
#include <OgreLogManager.h>
#include <Overlay/OgreOverlay.h>
#include <Overlay/OgreOverlayElement.h>
#include <Overlay/OgreOverlayManager.h>
#include <OgreRoot.h>
#include <OgreViewport.h>
#include <OgreSceneManager.h>
#include <OgreRenderWindow.h>
#include <OgreConfigFile.h>
#include <RTShaderSystem/OgreRTShaderSystem.h>
#include <OgreTimer.h>
#include <OgreFrameListener.h>

#define OGRE_STATIC_GLES2 1
#define OGRE_STATIC_OctreeSceneManager
#define OGRE_STATIC_ParticleFX
#define OGRE_STATIC_BSPSceneManager
//#define OGRE_STATIC_OctreeZone
//#define OGRE_STATIC_PCZSceneManager

#include <Plugins/OgreStaticPluginLoader.h>

/** This class demonstrates basic usage of the RTShader system.
 It sub class the material manager listener class and when a target scheme callback
 is invoked with the shader generator scheme it tries to create an equivalent shader
 based technique based on the default technique of the given material.
 */
class ShaderGeneratorTechniqueResolverListener : public Ogre::MaterialManager::Listener
{
public:
    
	ShaderGeneratorTechniqueResolverListener(Ogre::RTShader::ShaderGenerator* pShaderGenerator);
    
	/** This is the hook point where shader based technique will be created.
     It will be called whenever the material manager won't find appropriate technique
     that satisfy the target scheme name. If the scheme name is out target RT Shader System
     scheme name we will try to create shader generated technique for it. 
     */
	virtual Ogre::Technique* handleSchemeNotFound(unsigned short schemeIndex, const Ogre::String& schemeName, Ogre::Material* originalMaterial, unsigned short lodIndex, const Ogre::Renderable* rend);
protected:
	Ogre::RTShader::ShaderGenerator* mShaderGenerator;			// The shader generator instance.		
};

struct Camera
{
    Ogre::Vector3 velocity; // Current camera velocity in its local coordinate system.
    
    Ogre::Radian roll;
    Ogre::Radian pitch;
    Ogre::Radian yaw;
};

class OgreScene;

class OgreApplication
{
public:
	OgreApplication();
	~OgreApplication();
    
	void start(void* uiWindow, void* uiView, unsigned int width, unsigned int height);
    bool isStarted();
    void stop();
    
    void update(double timeSinceLastFrame);
    void draw();
    
    void resetCamera();
    
    Ogre::Timer mTimer;
    
    Ogre::RenderWindow* mRenderWindow;
    OgreScene* getOgreScene() const { return mOgreScene; }
    
private:
    OgreApplication(const OgreApplication&);
	OgreApplication& operator= (const OgreApplication&);
    
    void initializeRenderer(void* uiWindow, void* uiView, unsigned int width, unsigned int height);
    void terminateRenderer();
    
    void loadResources();
    
    void createScene();
    
    bool initializeRTShaderSystem();
    void terminateRTShaderSystem();
    
    Ogre::RTShader::ShaderGenerator* mShaderGenerator; // The Shader generator instance.
    ShaderGeneratorTechniqueResolverListener* mMaterialMgrListener; // Shader generator material manager listener.
    
    Ogre::Root* mRoot;
	Ogre::SceneManager* mSceneManager;
    OgreScene* mOgreScene;

    Ogre::String mResourcesRoot;
    
    Ogre::FrameEvent m_FrameEvent;
	int m_iNumScreenShots;
    
	bool m_bShutDownOgre;
	
    Ogre::StaticPluginLoader m_StaticPluginLoader;
};

#endif 
