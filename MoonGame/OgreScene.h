//
//  OgreScene.h
//  MoonGame
//
//  Created by Dahiri Farid on 3/14/15.
//  Copyright (c) 2015 Dahiri Farid. All rights reserved.
//

#ifndef __MoonGame__OgreScene__
#define __MoonGame__OgreScene__

#include <OgreRoot.h>
#include <OgreSceneManager.h>
#include <OgreCamera.h>
#include <OgreViewPort.h>
#include <Ogre/Terrain/OgreTerrain.h>
#include <Ogre/Terrain/OgreTerrainGroup.h>
#include <OgreLight.h>

class OgreScene
{
public:
    OgreScene(Ogre::Root* aRoot,
              Ogre::SceneManager* aSceneMgr);
    void start();
    void resetCamera();
    
    Ogre::SceneNode* getCameraYawNode() const { return mCameraYawSceneNode; }
    Ogre::SceneNode* getCameraPitchNode() const { return mCameraPitchSceneNode; }
    Ogre::SceneNode* getCameraSceneNode() const { return mCameraSceneNode; }
    void updateCamera(Ogre::Radian aRoll, Ogre::Radian aYaw);
    void setZoomSpeed(float aZoomSpeed) { mCameraZoomSpeed = aZoomSpeed; }
    float getZoomSpeed() const { return mCameraZoomSpeed; }
    void update(double aTimeSinceLastFrame);
    
protected:
    void configureTerrainGlobals(Ogre::Light* aLight);
    void defineTerrain(long aX, long aY);
    void initBlendMaps(Ogre::Terrain* terrain);
    void createLander();
    void createCamera();
    
private:
    Ogre::Root* mRoot;
    Ogre::SceneManager* mSceneManager;
    Ogre::Camera* mCamera;
    Ogre::Viewport* mViewport;
    Ogre::Entity* mLander;
    Ogre::SceneNode* mLanderSceneNode;
    Ogre::TerrainGlobalOptions* mTerrainGlobals;
    Ogre::TerrainGroup* mTerrainGroup;
    bool mTerrainsImported;
    Ogre::Real mCameraZoomSpeed;
    
    Ogre::SceneNode* mCameraContainerSceneNode;
    Ogre::SceneNode* mCameraPitchSceneNode;
    Ogre::SceneNode* mCameraYawSceneNode;
    Ogre::SceneNode* mCameraSceneNode;
};

#endif /* defined(__MoonGame__OgreScene__) */
